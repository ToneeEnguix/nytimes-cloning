// DESCRIPTION //

This repo was made as part of the Barcelona Code School Full-Stack bootcamp.

It is a clone of the New York Times website. (www.nytimes.com) using only HTML and CSS.

There is no link functionality but a mere cloning practice.

I found this cloning to be extremely useful for the further understanding of 'flex' and 'grid' properties in CSS.

// LIVE DEMO //

www.nytimescloning.surge.sh